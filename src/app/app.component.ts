import { Component } from '@angular/core';
import { BitlyClient } from 'bitly';
const bitly = new BitlyClient('b7704acb9e0947f95055f279ffd56cad76dfee4c', {});
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'url';
  url = '';
  shortUrl = '';
  constructor() {}
  geturl(e) {
    this.url = e.target.value;
  }
  async shorturl() {
    let result;
    try {
      result = await bitly.shorten(this.url);
      this.shortUrl = result.link;
      // debugger
    } catch (e) {
      throw e;
    }
    return result;
  }
}
